<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Event;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
       for ($i= 0;$i < 3;$i++){
           $event = new Event();
           $event
               ->setTitle($faker->title)
               ->setDescription($faker->text("200"))
               ->setImg($faker->imageUrl(640,480,'cats'))
               ->setStartsAt($faker->dateTimeBetween("-".($i+2)." days","-".($i+1)." days",null))
               ->setEndsAt($faker->dateTimeBetween(($i+1)." days",($i+2)." days",null));

               for($j = 0; $j < 4; $j++){
                   $user = new User();
                   $user
                       ->setEmail($faker->email)
                       ->setPassword($faker->password)
                       ->setName($faker->name)
                       ->setLastName($faker->lastName)
                       ->setRole('ROLE_USER');
                   $manager->persist($user);
                   for ($k=0;$k < 4;$k++){
                       $comment = new Comment();
                       $comment
                           ->setAuthor($user)
                           ->setText($faker->text(350))
                           ->setTitle($faker->title)
                           ->setEvent($event);
                       $manager->persist($comment);
                   }
               }

           $manager->persist($event);
       }

        $manager->flush();
    }
}
