<?php
namespace App\Controller;


use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/",name="home")
     */
    public function home():Response{
        return $this->render('pages/home.html.twig');
    }

    /**
     * @Route("/events",name="events")
     * @param EventRepository $repository
     * @return Response
     */
    public function events(EventRepository $repository):Response{
        $events = $repository->findBy([],['id' => 'ASC']);
        dump($events);
        return $this->render('pages/events.html.twig',[
            'events' => $events
            ]);

    }


}